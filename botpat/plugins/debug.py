"""Debugger module only usable by bot owner."""
from __future__ import annotations

from typing import Sequence
import datetime
import os
import platform
import time
from pathlib import Path


import hikari
import psutil
import tanjun
from tanjun.abc import SlashContext
from tanjun.checks import with_owner_check

component = tanjun.Component()

debug = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("debug", "A bunch of debugger commands.")
)  # pylint: disable=C0103


@debug.add_command
@with_owner_check
@tanjun.as_slash_command("clear-term", "Clear the dev terminal")
async def clear_term(ctx: SlashContext):
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")
    await ctx.respond("✅")


@debug.with_command
@tanjun.as_slash_command("about", "About this bot.")
async def about_bot(ctx: SlashContext, process: psutil.Process = tanjun.cached_inject(psutil.Process)):
    """Get the bot's current delay."""
    bot_user = await ctx.rest.fetch_my_user()
    start_time = time.perf_counter()
    rest_latency = (time.perf_counter() - start_time) * 1_000
    gateway_latency = ctx.shards.heartbeat_latency * 1_000 if ctx.shards else float("NAN")
    start_date = datetime.datetime.fromtimestamp(process.create_time())
    uptime = datetime.datetime.now() - start_date
    memory_usage: float = process.memory_full_info().uss / 1024**2
    cpu_usage: float = process.cpu_percent() / psutil.cpu_count()
    memory_percent: float = process.memory_percent()

    embed = (
        hikari.Embed()
        .set_thumbnail(bot_user.make_avatar_url())
        .set_author(name=f"About {bot_user.username}", url="https://gitlab.com/aster.codes/botpat/")
        .add_field(name="Uptime", value=f"{uptime}", inline=True)
        .add_field(
            name="Memory Usage",
            value=f"{memory_usage:.2f} MB ({memory_percent:.0f}%)",
            inline=True,
        )
        .add_field(name="CPU Usage", value=f"{cpu_usage:.2f}% CPU", inline=True)
        .add_field(name="REST Client Ping:", value=f"{rest_latency}", inline=True)
        .add_field(name="Gateway Client Ping:", value=f"{gateway_latency}", inline=True)
        .set_footer(
            text=f"Made with Hikari v{hikari.__version__}, Tanjun {tanjun.__version__}, and Python {platform.python_version()}",
        )
    )

    await ctx.respond(embed=embed)


redis = debug.with_command(tanjun.slash_command_group("redis", "Redis debugging commands."))  # pylint: disable=C0103


def all_plugins() -> Sequence[str]:
    plugins = [f"plugins.{p.name[:-3]}" for p in Path(__file__).resolve().parent.glob("*py") if p.name != "__init__.py"]
    return plugins


plugin_choices = all_plugins()


@debug.with_command
@tanjun.with_str_slash_option("module_name", "The module to target.", choices=plugin_choices)
@tanjun.as_slash_command("reload_module", "Reloads a module.")
async def reload_module(
    ctx: tanjun.abc.SlashContext,
    module_name: str,
    client: tanjun.Client = tanjun.injected(type=tanjun.Client),
):
    """Reload a module in Tanjun"""
    try:
        client.reload_modules(module_name)
    except ValueError:
        client.load_modules(module_name)

    await client.declare_global_commands()
    await ctx.respond("Reloaded!")


@debug.with_command
@with_owner_check
@tanjun.with_str_slash_option("module_name", "The module to target.")
@tanjun.as_slash_command("unload_module", "Removes a module.")
async def unload_module(
    ctx: tanjun.abc.SlashContext, module_name: str, client: tanjun.Client = tanjun.injected(type=tanjun.Client)
):
    """Unload a module in Tanjun"""
    try:
        client.unload_modules(module_name)
    except ValueError:
        await ctx.respond("Couldn't unload module...")
        return

    await client.declare_global_commands()
    await ctx.respond("Unloaded!")


@debug.with_command
@with_owner_check
@tanjun.with_str_slash_option("module_name", "The module to reload.")
@tanjun.as_slash_command("load_module", "Loads a module.")
async def load_module(
    ctx: tanjun.abc.SlashContext, module_name: str, client: tanjun.Client = tanjun.injected(type=tanjun.Client)
):
    """Load a module in Tanjun"""
    try:
        client.load_modules(module_name)
    except ValueError:
        await ctx.respond("Can't find that module!")
        return

    await client.declare_global_commands()
    await ctx.respond("Loaded!")


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
