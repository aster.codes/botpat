import asyncio
from datetime import timedelta

import hikari
import yuyo
import tanjun

from util.constants import SERVER_RULES, CHANNEL_RULES


component = tanjun.Component()

rules = component.with_slash_command(
    tanjun.slash_command_group("rules", "Check and provide Server or Channel rules.")
)  # pylint: disable=C0103  # pylint: disable=C0103


@rules.add_command
@tanjun.with_str_slash_option("rule", "The server rule to view", choices=list(SERVER_RULES.keys()))
@tanjun.as_slash_command("server-rules", "Check a server rule.")
async def server_rules(
    ctx: tanjun.abc.SlashContext,
    rule: str,
):
    guild = ctx.get_guild()
    if not guild:
        await ctx.fetch_guild()

    rule_entry = SERVER_RULES[rule]
    embed = (
        hikari.Embed(
            title=rule,
            description=rule_entry.get("description") if rule_entry.get("description") else None,
            color=rule_entry.get("color") if rule_entry.get("color") else 000000,
        )
        .set_thumbnail(rule_entry.get("thumbnail") or guild.icon_url if guild else None)
        .set_author(name="BotPat wants you to read the SERVER_RULES")
    )
    embed_fields = rule_entry.get("fields", {})

    for name, body in embed_fields.items():
        if name not in ["description", "color"]:
            embed.add_field(name=name, value=body, inline=False)

    await ctx.respond(embed)


@rules.add_command
@tanjun.as_slash_command("channel-rules", "Check a channel rule.")
async def channel_rules(
    ctx: tanjun.abc.SlashContext,
    component_client: yuyo.ComponentClient = tanjun.inject(type=yuyo.ComponentClient),
):
    channel_entry = CHANNEL_RULES.get(str(ctx.channel_id), {})
    if not channel_entry:
        return
    guild = ctx.get_guild()
    if not guild:
        await ctx.fetch_guild()

    row = (
        ctx.rest.build_action_row()
        .add_select_menu("rule-menu")
        .set_placeholder(f"What Rule would you like to view for {guild.name}?")
    )

    entry_fields = channel_entry.get("fields", {})
    for name, body in entry_fields.items():
        if name not in ["description", "color"]:
            row.add_option(name, name).set_description(body[:99]).add_to_menu()

    message = await ctx.respond("Select a Rule:", component=row.add_to_container(), ensure_result=True)
    executor = yuyo.WaitFor(authors=(ctx.author.id,), timeout=timedelta(minutes=3))

    component_client.set_executor(message, executor)

    try:
        result = await executor.wait_for()
        choice = result.interaction.values[0]
    except asyncio.TimeoutError:
        return

    embed = (
        hikari.Embed(title=choice, description=entry_fields.get(choice), color=channel_entry.get("color", 000000))
        .set_thumbnail(guild.icon_url)
        .set_author(name="BotPat wants you to read the SERVER_RULES")
    )
    await ctx.edit_initial_response(content=choice, embed=embed, components=[])


loader = component.make_loader()
