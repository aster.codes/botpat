from __future__ import annotations

import hikari
import tanjun

from tanjun.abc import SlashContext


STAFF_FEEDBACK = 911826603896606730
BOT_TEST1 = 794428811206066226
RESPOND_CHANNEL = STAFF_FEEDBACK

GENDER_SAFE_TALK = 658807627576246272
TW_VENT = 465920060872065024
CASUAL_VENT = 428919368810889219
CURRENT_EVENTS = 796488552262795285

SRS_CHANNELS = [CURRENT_EVENTS, CASUAL_VENT, TW_VENT, GENDER_SAFE_TALK]

SRS_FILTER = [
    "applebee",
    "anonymous",
]

component = tanjun.Component()


@component.with_listener(hikari.GuildMessageCreateEvent)
async def srs_listener(event: hikari.GuildMessageCreateEvent):
    """Doc String"""
    if not event.member or event.is_bot or not event.message.content:
        return

    if event.channel_id not in [*SRS_CHANNELS, RESPOND_CHANNEL]:
        return

    if matches := list(filter(lambda cont: cont in event.message.content.lower(), SRS_FILTER)):
        await event.app.rest.create_message(
            RESPOND_CHANNEL,
            embed=hikari.Embed(
                title="Warning",
                description=f"[This message]({event.message.make_link(event.guild_id)}) mentioned {list(matches)}.",
            )
            .set_author(icon=hikari.Emoji.parse("🤔"), name=f"{event.member.username}#{event.member.discriminator}")
            .add_field(name="Channel ID:", value=str(event.channel_id))
            .add_field(name="Message ID:", value=str(event.message_id))
            .add_field(name="Author ID:", value=str(event.member.id)),
        )


loaders = component.make_loader()
