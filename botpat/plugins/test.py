from __future__ import annotations

import tanjun
import hikari

from checks.role_checks import with_any_role_check


@with_any_role_check('Mods')
@tanjun.with_channel_slash_option("to_channel", "The channel to send to", default=None)
@tanjun.with_str_slash_option("to_say", "What the bot will say")
@tanjun.as_slash_command("bot_say", "Make the bot say a thing")
async def slash_command(ctx: tanjun.abc.Context, to_say: str, to_channel: hikari.InteractionChannel | None) -> None:
    if to_channel:
        await ctx.rest.create_message(to_channel.id, to_say)
        return

    await ctx.respond(to_say)


component = tanjun.Component().load_from_scope().make_loader()
