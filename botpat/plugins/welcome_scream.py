import datetime
from random import choice

import hikari
import tanjun

component = tanjun.Component()


WELCOME_GIFS = [
    "https://cdn.discordapp.com/attachments/838613222092963871/903104333414006834/screaming_peaches.gif",
    "https://tenor.com/view/welcome-come-in-come-on-in-hello-greetings-gif-14341111",
    "https://tenor.com/view/spongebob-welcome-club-patrick-squidward-gif-8172094",
    "https://tenor.com/view/whale-hello-there-how-to-speak-irish-its-nice-to-meet-you-hello-there-hello-gif-15571605",
    "https://tenor.com/view/hello-there-hi-greetings-princess-bride-mandy-patinkin-gif-17710385",
    "https://tenor.com/view/nice-to-meet-you-gif-11646032",
    "https://tenor.com/view/hello-there-baby-yoda-mandolorian-hello-gif-20136589",
    "https://tenor.com/view/jungle-welcome-vocalist-band-gif-7617009",
    "https://tenor.com/view/hello-there-hi-there-greetings-gif-9442662",
]


@component.with_listener(hikari.GuildMessageCreateEvent)
async def welcome_gif(event: hikari.GuildMessageCreateEvent):
    channel = event.get_channel()
    if (
        not channel
        or not channel.name.startswith("general")
        or not event.member
        or channel.name.startswith("meet-and-greet")
    ):
        return

    now = datetime.datetime.now().astimezone()
    member_timer = event.member.joined_at + datetime.timedelta(hours=24)
    if now > member_timer:
        return

    history_messages = await channel.fetch_history(after=event.member.joined_at).filter(("author", event.member))
    if len(history_messages) == 1:
        await channel.send(
            choice(WELCOME_GIFS),
            reply=history_messages[0],
            user_mentions=True,
        )


@component.with_listener(hikari.MemberCreateEvent)
async def welcome_message(
    event: hikari.MemberCreateEvent, cache: hikari.api.Cache = tanjun.inject(type=hikari.api.Cache)
):
    MESSAGE = (
        f"Hello {event.member.mention}, Welcome to the Official Game Theorists!\n\n"
        "We’re so happy to have you here. Make sure to read <#483967686540394506> and"
        "<#1224036069692014602> and enjoy the server. If you have any questions feel free"
        " start a ticket with <#923856791215964160>!"
        " You can also grab some roles in <id:customize>; we have ones for gender,"
        " getting notified when new things happen, and comfort."
    )
    channel = cache.get_guild_channel(394732370282151936)
    if not channel:
        channel = await event.app.rest.fetch_channel(394732370282151936)
    await channel.send(MESSAGE, user_mentions=True)


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
