import re
import subprocess

import nox

LOCATIONS = ["botpat", "noxfile.py"]

nox.options.sessions = [
    "reformat_code",
    "spell_check",
    # "type_check_code",
    "flake8_code",
    # "pylint_code",
    # "pytest_code",
]


@nox.session(reuse_venv=True)
def type_check_code(session: nox.Session):
    """Run PyRight to Type Check code."""
    session.install("pyright")
    session.run("pyright", *LOCATIONS[:-1])


@nox.session(reuse_venv=True)
def reformat_code(session: nox.Session):
    """Run Black and isort to format code."""
    session.install("black")
    session.install("isort")
    session.run("black", *LOCATIONS)
    session.run("isort", *LOCATIONS)


@nox.session(reuse_venv=True)
def pylint_code(session: nox.Session):
    """Run PyLint  to lint code."""
    session.install("pylint")
    session.install("anybadge")
    session.install("pylint-exit")
    session.run("rm", "-rf", "./pylint")
    session.run("mkdir", "./pylint")
    pylint_result = subprocess.run(
        [
            "poetry",
            "run",
            "pylint",
            "botpat",
        ],
        capture_output=True,
    )
    pylint_result = pylint_result.stdout.decode("utf-8")
    pylint_score_re = r"^Your code has been rated at ([\d\.\/]+)"
    match = re.search(pylint_score_re, pylint_result, re.MULTILINE)
    session.run(
        "anybadge",
        "--label=Pylint",
        "--file=pylint/pylint.svg",
        f"--value={match.group(0)}",
        "2=red",
        "4=orange",
        "8=yellow",
        "10=green",
    )
    session.run("bash", "-c", f'echo "Pylint score is {match.group(0)}"')


@nox.session(reuse_venv=True)
def flake8_code(session: nox.Session):
    """Run Flake8  to lint code."""
    session.install("flake8")
    session.run("flake8", *LOCATIONS[:-1])


@nox.session(reuse_venv=True)
def prune_code(session: nox.Session):
    """Run Vulture  to find duplicate code."""
    session.install("vulture")
    session.run("vulture", *LOCATIONS[:-1])


@nox.session(reuse_venv=True)
def spell_check(session: nox.Session):
    """Run codespell to spell check code."""
    session.install("codespell")
    session.run("codespell", *LOCATIONS, "README.md")


@nox.session(reuse_venv=True)
def pytest_code(session: nox.Session):
    """Run pytest to test code."""
    session.install("pytest")
    session.run(
        "pytest",
        *LOCATIONS,
    )
    session.run("coverage", "xml")


@nox.session(reuse_venv=True)
def clean_up(session: nox.Session):
    """Cleanup build directories."""
    session.run("rm", "-R", "coverage_html/", "docs/")
