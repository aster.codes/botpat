FROM python:3.9.5-slim

# Make python print to docker stdout
ENV PYTHONUNBUFFERED 1

# Setup our working directory
WORKDIR /app/botpat

RUN ls -lah
RUN pwd
# Update system , install git, upgrade pip
RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install build-essential git -y
RUN pip install --upgrade pip
RUN pip install pip-tools

# Install Poetry and Setup
COPY ./requirements .
RUN pip-compile requirements.in dev-reqs.in --output-file requirements.txt
RUN pip-sync

# Start the bot
CMD ["python", "run.py"]
