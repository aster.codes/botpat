"""Debugger module only usable by bot owner."""
from __future__ import annotations

import hikari
import tanjun
from tanjun.abc import SlashContext

component = tanjun.Component()

about = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("about", "Provides some commands about MatPat")
)


@about.with_command
@tanjun.with_member_slash_option("to_ping", "The member to ping/reply to.", default=None)
@tanjun.as_slash_command("is-matpat-here", "I'll show an embed with all of MatPat's official links.")
async def is_matpat_here(ctx: SlashContext, to_ping: hikari.InteractionMember):
    """Provide a nice embed with all of MatPats official information"""
    desc = (
        "As the [first message](https://discord.com/channels/353694095220408322/"
        "483967686540394506/735637644087656518) in <#483967686540394506> states, "
        "MatPat does not have Discord account. We are a community made and run "
        "server that MatPat has endorsed in his videos. For reaching out about "
        "theories and game suggestions, you should head to his [Twitter]"
        "(https://twitter.com/MatPatGT) or use the [Reddit Megathread](https://"
        "www.reddit.com/r/GameTheorists/comments/lezw1o/theory_suggestions_"
        "megathread/) instead.\n\nBelow we have gathered a list of Official links."
        " These are links to official Social Media, Channels, websites, and communities"
        " MatPat has official mentioned or endorsed!"
    )
    embed = hikari.Embed(title="Is MatPat in this server?", description=desc)

    embed.add_field(name="Official Social Media!", value="-------------------------", inline=False)
    embed.add_field(
        name="MatPats Twitter",
        value="[@MatPatGT](https://twitter.com/MatPatGT/)",
        inline=True,
    )
    embed.add_field(
        name="Stephs Twitter",
        value="[@CordyPatrick](https://twitter.com/cordypatrick)",
        inline=True,
    )
    embed.add_field(
        name="MatPats Facebook",
        value="[MatPat's FB](https://www.facebook.com/GameTheorists)",
        inline=True,
    )
    embed.add_field(
        name="MatPats Instagram",
        value="[@MatPatGT](https://www.instagram.com/matpatgt/)",
        inline=True,
    )
    embed.add_field(
        name="Official Youtube Channels!",
        value="-------------------------------",
        inline=False,
    )
    embed.add_field(
        name="Game Theory",
        value=(
            "[Game Theory Official](https://www.youtube.com/c/GameTheorists/)\n"
            "Game Theory About: [About Page]"
            "(https://www.youtube.com/c/GameTheorists/about)\n"
            "(multiple links to official socials)"
        ),
        inline=True,
    )
    embed.add_field(
        name="Film Theory",
        value="[Film Theory Official](https://www.youtube.com/c/FilmTheorists)",
        inline=True,
    )
    embed.add_field(
        name="Food Theory",
        value="[Food Theory](https://www.youtube.com/c/FoodTheorists)",
        inline=True,
    )
    embed.add_field(
        name="MatPats Official Business Links",
        value="-------------------------------------",
        inline=False,
    )
    embed.add_field(
        name="MatPats Store",
        value="[CreatorInk](https://creatorink.com/collections/theory-wear-by-matpat)",
        inline=True,
    )
    embed.add_field(
        name="MatPat's Business Email",
        value="||[theorist@longhaulmgmt.com](mailto:theorist@longhaulmgmt.com)||",
        inline=True,
    )
    embed.add_field(
        name="MatPat's Officially Endorsed Links",
        value="--------------------------------------",
        inline=False,
    )
    embed.add_field(
        name="Subreddit",
        value="[/r/GameTheorists](https://www.reddit.com/r/gametheorists)",
        inline=True,
    )
    embed.add_field(
        name="Community Discord",
        value=(
            "[Invite Link](https://discord.gg/gametheorists)\n(That's Here!!)\n"
            "[The Theorist Gateway page that mentions us](https://www."
            "thetheoristgateway.com/tenretniolleh/getting-started.html)"
        ),
        inline=True,
    )

    embed.set_thumbnail(
        "https://cdn.discordapp.com/attachments/376110622942691329/" "893699661079134259/5f7352260ab50d00184acfe5.png"
    )
    embed.set_footer(
        text="Please make sure to read the rules. We work really hard on those :(",
        icon=ctx.get_guild().icon_url,
    )

    if to_ping:
        await ctx.respond(
            content=f"{to_ping.mention} it looks like you asked about my accounts! Here's some info:",
            embed=embed,
            user_mentions=True,
        )
        return
    await ctx.respond(
        content="It looks like someone asked about my accounts! Here's some info:",
        embed=embed,
    )


loaders = component.make_loader()
