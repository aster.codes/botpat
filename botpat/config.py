"""Basic implementation for Configs"""
from __future__ import annotations

import abc
import collections.abc as collections_abc
import logging
import os
import typing
from dataclasses import dataclass

import hikari
from hikari import CacheComponents

__all__: list[str] = ["FullConfig", "Tokens"]


ConfigT = typing.TypeVar("ConfigT", bound="Config")
DefaultT = typing.TypeVar("DefaultT")
ValueT = typing.TypeVar("ValueT")


class Config(abc.ABC):
    """Provides an implementation for the Basic Configuration."""

    __slots__ = ()

    @classmethod
    @abc.abstractmethod
    def from_environ(cls: type[ConfigT]) -> ConfigT:
        """Checks local environment variables and attempts to build the Config."""
        raise NotImplementedError


@dataclass(repr=False)
class Tokens(Config):
    """Provides an implementation for the Token Configuration."""

    discord_bot: str

    @classmethod
    def from_environ(cls) -> Tokens:
        if not os.environ.get("DISCORD_TOKEN"):
            raise RuntimeError("Must provide discord bot token in environment variable DISCORD_TOKEN.")

        return cls(
            discord_bot=os.environ.get("DISCORD_TOKEN"),
        )


DEFAULT_CACHE = (
    CacheComponents.GUILDS
    | CacheComponents.GUILD_CHANNELS
    | CacheComponents.ROLES
    | CacheComponents.MEMBERS
    | CacheComponents.EMOJIS
    | CacheComponents.MESSAGES
    | CacheComponents.VOICE_STATES
)


@dataclass(repr=False)
class FullConfig(Config):  # pylint: disable=R0902
    """Provides a class and methods to handle BotPat's Configuration."""

    tokens: Tokens
    cache: hikari.CacheComponents = DEFAULT_CACHE
    intents: hikari.Intents = hikari.Intents.ALL_UNPRIVILEGED
    log_level: int | str | dict[str, typing.Any] | None = logging.INFO
    mention_prefix: bool = True
    owner_only: bool = False
    prefixes: collections_abc.Set[str] = frozenset("m;")
    set_global_commands: typing.Union[bool, hikari.Snowflake] = True

    @classmethod
    def from_environ(cls) -> FullConfig:
        log_level = os.environ.get("log_level", "INFO")
        if not isinstance(log_level, (str, int)):
            raise ValueError("Invalid log level found in config")

        log_level = log_level.upper()

        set_global_commands = os.environ.get("declare_global_commands", True)
        if not isinstance(set_global_commands, bool):
            set_global_commands = hikari.Snowflake(set_global_commands)

        return cls(
            tokens=Tokens.from_environ(),
            cache=DEFAULT_CACHE,
            intents=typing.cast(
                hikari.Intents,
                os.environ.get("DISCORD_INTENTS", hikari.Intents.ALL_UNPRIVILEGED),
            ),
            log_level=log_level,
            mention_prefix=bool(os.environ.get("MENTION_PREFIX", True)),
            owner_only=bool(os.environ.get("OWNER_ONLY", False)),
            prefixes=set(os.environ.get("PREFIXES", "m;").split(",")),
            set_global_commands=set_global_commands,
        )
