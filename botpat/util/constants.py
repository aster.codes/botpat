SERVER_RULES = {
    "Rule 1: Be Nice, Be Respectful": {
        "fields": {
            "This should go without saying, but be respectful.": "Discussion and debate are allowed (in the appropriate channels) "
            "but civility, respect and consideration are always required!",
            "Hatred and Bigotry": "No sexism, no racism, no bigotry, no homophobia, no transphobia,"
            "no ableism, no slurs, no glorifying or advocating violence, no harassments, "
            "or bullying, participating only constructively in comment threads, accepting "
            "criticism and allowing diversity of opinions. In summary, you should be nice "
            "and respectful to others while asking questions or sending messages.",
            "Comfort Roles, DM's and Pings": "In this server, we have Comfort Roles. this is a role listed in a user's role "
            "list where they can specify whether they want to be pinged or Direct Messaged/"
            "DM'd. Do not Direct Message or Ping any server member, including staff, without "
            "asking them or if their server roles specify otherwise. In the case of needing "
            "help from server staff or Mods, you can ask for assistance in <#516027780908056578>",
        }
    },
    "Rule 2: Acceptable Content": {
        "fields": {
            "No Roleplaying": "Roleplay, also known as RP, is not permitted anywhere in the server. Small actions such as "
            "-hugs- and -cries- are tolerated, but if it progresses into a wide-scale RP chat, you will be "
            "asked to stop.",
            "No Spamming": "Unreasonably direct messaging or pinging members or being disruptive in any chat is not allowed. "
            "Additionally, please never ping the GT Cast. If you need help or have a question related to the "
            "server, you can ask in server-help. In general, any unnecessary pings anywhere should be avoided.",
            "No Shitposting": "'Shitposting' describes any messages that disrupt a conversation or channel with humorous "
            "intent. We have a channel dedicated to this, memes-and-mischief, so shitposts are not allowed "
            "elsewhere. Additionally, spam is prohibited in that channel as well.\n\nCopypastas and "
            "Insensitive/Otherwise rule-breaking shitposting are not permitted anywhere. The same goes for "
            "posting an unreasonable amount of emotes/only using emotes instead of text, etc.",
            "No NSFW PG13+ Content": "As a rule of thumb, the general content in this server should be around PG-13. Sharing "
            "any messages or content that would be unsuited for younger members is not allowed. This "
            "includes pictures, links, videos, slurs, excessive swearing, abusing emoji/reactions, "
            "and gore. There is a banned word list pinned in server-help.\n This may result in an "
            "immediate Ban.\n This rule also includes NSFW (or otherwise rule-breaking) Profile "
            "Pictures, Custom Statuses, and Usernames.",
        }
    },
    "Rule 3: Sensitive topics": {
        "description": "There is a Serious Topics category where you can discuss serious topics or vent. However, make "
        "sure to spoiler potentially triggering content and add a content warning. Please note that while discussion of"
        "sensitive topics is allowed, glorification or encouragement of unhealthy, dangerous, and/or harmful behavior "
        "is strictly forbidden and warrants a ban. "
        "Insensitivity towards serious topics, especially when it’s about another person, is not tolerated and includes"
        "everything from Rule 1."
    },
    "Rule 4: No self-promotion without consent of a moderator": {
        "description": "DMing people about your content without them specifically asking you to, counts as self-promo. This "
        "includes asking people to join Discord Servers and sharing your own videos.\n\n"
        "Linking to your profile from anywhere is not allowed. Do not share content that you are in any way "
        "involved with or related to. Includes crowdsourcing pages like gofundme.\n\n"
        "Calls to action such as “check out my channel”, “leave a like”, etc are also self-promo.\n\n"
        "Only links to trusted/popular websites such as YouTube, Soundcloud, Twitter, etc. are allowed. Additionally,"
        "make sure to check the channel pins to see what is allowed and what is not.\n\n"
        "If you believe to have an important link that would fall under the self-promo rules, you can ask in \n\n"
        "server-help to DM a mod and ask for approval to post it."
    },
    "Rule 5. No penalty evasion": {
        "description": "Using multiple accounts or leaving and rejoining the server to attempt to get around "
        "penalties is not allowed under any circumstances. You are allowed to use multiple accounts on the "
        "server, but using them to avoid punishments (or double potential rewards) is not allowed."
    },
    "Rule 6: Remember Discord Terms of Service": {
        "description": "We are required to enforce Discord’s Terms of Service. The most common for our server members are:",
        "No Raiding": "Brigading/Raiding other subreddits or other Discord servers is breaking TOS. Any users caught "
        "brigading will be banned and reported.",
        "Minimum Age 13": "According to Discord ToS, you must be 13 to have an account. If you are under 13, we must ban you.",
    },
    "Rule 7: Keep channels on topic": {
        "description": "There are a lot of channels with specific topics, so if you want to discuss a certain "
        "topic please move to the appropriate channel. If you are unsure what a channel is for you "
        "can click on it and read the channel topic at the top (on PC) or on the sidebar (on mobile). If you "
        "are still unsure you can ask in server-help.\n\n"
        "Failure to follow when a Staff member asks you to move to a channel will result in a warn/mute."
    },
    "Rule 8: No impersonation": {
        "description": "Pretending to be another person without their consent is forbidden. This includes Game Theory "
        "members like MatPat, other celebrities, discord employees, and staff members of this server."
    },
}

THEORY_CATEGORY_RULES = {
    "Be Nice, Be Respectful": """If you've ever been on the subreddit, you'll know this rule. When someone posts something, you are expected to be kind and respectful to them, even if you don't agree with their theory--their theory won't discredit yours, so don't attack other people with different theories. If you want to give feedback or criticism, it must be in a constructive and kind manner; no insults allowed. Finally, please do not go into a channel just to attack the game or company. Any such behavior will lead to your removal from whatever channel you are insulting.""",
    "Quality Control": """Memes and shitpost theories belong in #memes-and-mischief.\n\nDo not discuss your own gameplay, ask to play with others, or post screenshots unless they are purely for theorizing. These channels are not for gaming, #gaming is.\n\nLow-quality theories (bad spelling, unreadable grammar, low research, already proven wrong, low on content etc.) are also governed by this rule. As our terms guide states, a theory must have a hypothesis and supporting evidence. You can't just say ''I think (x) is (y)'' and leave it at that.""",
    "Official Theory Videos and MatPat": """We already have places to discuss Official Videos-- #game-theory-videos, #food-theory-videos, and #film-theory-videos-- so please use those, not these channels if you wish to talk about the videos.\n\nAdditionally, please don’t use MatPat to defend your theory. “MatPat said (x).” We love and enjoy Mat's content, but we also acknowledge that he is neither infallible nor the creator of every game he theorizes on--MatPat is not fact, and he cannot establish something as a fact. You can use his research to build your own theories, but canon facts will overrule MatPat theories in these channels""",
    "Formatting and Research": """Please put long theories into a google doc. This is to reduce spam in a channel.  Make sure your theory doc is legible, well-formatted, and sound in grammar and spelling before posting.\n\nWhen you are theorizing, you are expected to research and be well-versed in the topic you choose. Before posting, make sure you have evidence for your theory and that you understand the canon and facts well enough to support and defend your theories, and always remember to cite your sources and check source reliability. We understand that you won't know everything and will help you if you miss something, but we expect you to at least know the basics.""",
    "Spoilers": """For game leaks, it is necessary to have images and information in spoilers. For official announcements and released games, spoilers must be used for the time period determined by the ATs.\n\nIf you don't know how to spoiler, it's an automatic option for images on desktop (if you're on mobile, have someone on desktop post it for you), and for text, it looks like this `||(insert text here)||` or using /spoiler.""",
    "Serious Subjects": """At this time, there are certain subjects that cannot be theorized on or require respect when handling.\n\n__**Banned topics**__\n- Theorizing using US officially-recognized religions\n- Theorizing about real-life current tragedies\n- Conspiracy theories in general\n\n__**'Handle With Care' Topics**__\n- Disabilities and Mental Health\n- Historical Events""",
}

CHANNEL_RULES = {
    "794428811206066226": {  # theory-discussion
        "fields": THEORY_CATEGORY_RULES,
        "color": 0xFFEA00,
    },
    "821467250606342234": {  # theory-discussion-2
        "fields": THEORY_CATEGORY_RULES,
        "color": 0xFFEA00,
    },
    "905220117569355807": {  # just-fnaf
        "fields": THEORY_CATEGORY_RULES,
        "color": 0xFFEA00,
    },
    "604346935029268480": {  # minecraft
        "fields": THEORY_CATEGORY_RULES,
        "color": 0xFFEA00,
    },
    "714295314202361886": {  # tv-and-movie-theories
        "fields": THEORY_CATEGORY_RULES,
        "color": 0xFFEA00,
    },
    "354405657484460043": {  # art-and-design
        "fields": {
            "Be Nice, Be Respectful": """If you've ever been on the subreddit, you'll know this rule. When someone posts something, you are expected to be kind and respectful to them, even if you don't agree with their theory--their theory won't discredit yours, so don't attack other people with different theories. If you want to give feedback or criticism, it must be in a constructive and kind manner; no insults allowed. Finally, please do not go into a channel just to attack the game or company. Any such behavior will lead to your removal from whatever channel you are insulting.""",
            "Quality Control": """Memes and shitpost theories belong in #memes-and-mischief.\n\nDo not discuss your own gameplay, ask to play with others, or post screenshots unless they are purely for theorizing. These channels are not for gaming, #gaming is.\n\nLow-quality theories (bad spelling, unreadable grammar, low research, already proven wrong, low on content etc.) are also governed by this rule. As our terms guide states, a theory must have a hypothesis and supporting evidence. You can't just say ''I think (x) is (y)'' and leave it at that.""",
            "Official Theory Videos and MatPat": """We already have places to discuss Official Videos-- #game-theory-videos, #food-theory-videos, and #film-theory-videos-- so please use those, not these channels if you wish to talk about the videos.\n\nAdditionally, please don’t use MatPat to defend your theory. “MatPat said (x).” We love and enjoy Mat's content, but we also acknowledge that he is neither infallible nor the creator of every game he theorizes on--MatPat is not fact, and he cannot establish something as a fact. You can use his research to build your own theories, but canon facts will overrule MatPat theories in these channels""",
            "Formatting and Research": """Please put long theories into a google doc. This is to reduce spam in a channel.  Make sure your theory doc is legible, well-formatted, and sound in grammar and spelling before posting.\n\nWhen you are theorizing, you are expected to research and be well-versed in the topic you choose. Before posting, make sure you have evidence for your theory and that you understand the canon and facts well enough to support and defend your theories, and always remember to cite your sources and check source reliability. We understand that you won't know everything and will help you if you miss something, but we expect you to at least know the basics.""",
            "Spoilers": """For game leaks, it is necessary to have images and information in spoilers. For official announcements and released games, spoilers must be used for the time period determined by the ATs.\n\nIf you don't know how to spoiler, it's an automatic option for images on desktop (if you're on mobile, have someone on desktop post it for you), and for text, it looks like this `||(insert text here)||` or using /spoiler.""",
            "Serious Subjects": """At this time, there are certain subjects that cannot be theorized on or require respect when handling.\n\n__**Banned topics**__\n- Theorizing using US officially-recognized religions\n- Theorizing about real-life current tragedies\n- Conspiracy theories in general\n\n__**'Handle With Care' Topics**__\n- Disabilities and Mental Health\n- Historical Events""",
        },
        "color": 0xFF799F,
    },
    "353766146115371009": {  # memes-and-mischief
        "fields": {
            "Server Rules Apply": """ All of the rules listed in #server-rules apply.\n\nThis especially includes Slurs, Copypastas, and Roleplaying not being allowed.""",
            "Not Allowed List": """__All the following is **not** allowed__:\n\n- Memes about Serious Topics (including, but not necessarily limited to, all topics listed in #unlock-serious-category). If you are unsure about whether or not a meme you want to post covers a serious topic, you can ask in #server-help to DM an Expert Meme-er.\n- Excessively dark/offensive humor.\n- Memes concerning Religion.\n- Intentional Reposting/Stealing of Memes.\n- Avoiding the perm system by posting links to memes when below New Theorist.\n- Intentional Reposting.\n- Posting “random”/funny screenshots from this server. That is what #hall-of-no-context is for.\n- Asking for memes to be pinned (even if they’re not your own).\n- Excessive Self Deprecation.""",
        },
        "color": 0xB38CF1,
    },
}
