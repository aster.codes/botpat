# BotPat

<div align="center">

[![pipeline status](https://gitlab.com/aster.codes/botpat/badges/main/pipeline.svg)](https://gitlab.com/aster.codes/botpat/-/pipelines)
[![PyLint](https://gitlab.com/aster.codes/botpat/-/jobs/artifacts/main/raw/pylint/pylint.svg?job=PyLint%20Lint)](https://gitlab.com/aster.codes/botpat/-/jobs/artifacts/main/raw/pylint/pylint.log?job=PyLint%20Job)
[![Black Formatted](https://img.shields.io/badge/Formatting-Black-000?logo=python&logoColor=ffd343)](https://gitlab.com/aster.codes/botpat/-/commits/main)
[![Python Support](https://img.shields.io/badge/Python%20Support-%5E3.9.5-orange?logo=python&logoColor=ffd343)](https://gitlab.com/aster.codes/botpat/-/commits/main)
[![Discord Support Server](https://img.shields.io/discord/794422514935529493?color=5865F2&label=Discord&logo=discord&logoColor=white)](https://discord.gg/Cx65DkJZtM)
[![Docker Compose Support](https://img.shields.io/badge/Docker%20Compose-Build%20Ready!-blue?logo=docker&logoColor=white)](https://gitlab.com/aster.codes/botpat/-/blob/main/docker-compose.yml)

</div>


This bot was made for [the Official Game Theorists Discord](https://discord.gg/gametheorists) ![Game Theory DiscorD](https://img.shields.io/discord/353694095220408322?color=7289da&label=Used%20in&logo=discord&logoColor=white).

It's only purpose is to provide some simple information on MatPat and funny reaction images.

